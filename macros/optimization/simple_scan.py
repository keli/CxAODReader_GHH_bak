import numpy as np
import ROOT
import time
import math
from array import array
#######################################################
# Input
#######################################################
inputfile = "MjjFull.root"
#bkgs = ["ttbar","Zjets","Vgamma"]
bkgs = ["ttbar", "Wjets", "Zjets", "Vgamma","WZ","ZZ","ttW","ttZ","tZ","VVV","ssWW","SingleTop"]
sigs = ["WWW"]
chs = {"ee": '0', "e#mu": '1', "#mue": '2', "#mu#mu": '3'}
#chs = {}
#chs["inclusive"] = 'Inc'
variables = {}
variables["LepPLVEl"] = [["-inf", x] for x in np.arange(0,-0.95, -0.05)]
variables["LepPLVMu"] = [["-inf", x] for x in np.arange(0,-0.95, -0.05)]
variables["LepECIDSBDT"] = [[x, "inf"] for x in np.arange(-0.3,0.95, 0.05)]


#######################################################
# Run
#######################################################

start_time = time.time()

def GetIntegral( plot, variable_range):
  if variable_range[0] == '-inf':
    bin_low = 0
  else:
    bin_low = plot.FindBin(variable_range[0])
  
  if variable_range[1] == 'inf':
    bin_high = -1
  else:
    bin_high = plot.FindBin(variable_range[1])
  return plot.Integral(bin_low,bin_high)

##############################
if __name__ == '__main__':
  ROOT.gStyle.SetOptStat(0)
  ROOT.gROOT.SetBatch(1)

  for ch in chs:
    print("------------------------------------------------------------------")
    print("Channel: "+ch.strip('#'))  
    sig_all_total_num = 0.0
    sig_each_total_num = {}
    bkg_all_total_num = 0.0
    bkg_each_total_num = {}
    
    print("--- INPUT ---")
    region = ROOT.TFile(inputfile,"open")
    print("Input File: "+inputfile) 

    variable = variables.keys()[0]  

    print("Signals: ") 
    for sig in sigs:
      sig_each_total_num[sig] = GetIntegral(region.Get("Nominal/"+variable+"_"+sig+"_"+chs[ch]), ['-inf','inf'])
      print(sig)
      sig_all_total_num += sig_each_total_num[sig] 

    print("Backgrounds: ")
    for bkg in bkgs:
      bkg_each_total_num[bkg] = GetIntegral(region.Get("Nominal/"+variable+"_"+bkg+"_"+chs[ch]),['-inf','inf'])
      print(bkg)
      bkg_all_total_num += bkg_each_total_num[bkg]

    print("----- Before Any Cut ------")
    print("Total Signal Events: %f" % (sig_all_total_num))
    print("Total Background Events: %f" % (bkg_all_total_num))
    significance = sig_all_total_num/math.sqrt(sig_all_total_num + bkg_all_total_num)
    print("S/Sqrt(S+B): %f" % (significance))
    print 

    print("--- Single Value Scan ---")
    for variable in variables:
      print("-------------------------")
      print("Cut Variable: "+variable)
      print 

      cuts = []
      sig_effs = []
      bkg_rejs = []
      significances = []
      

      for variable_range in variables[variable]:
        print("Cut Range: "+ str(variable_range))
        sig_all_num = 0.0
        bkg_all_num = 0.0

        for sig in sigs:
          sig_all_num += GetIntegral(region.Get("Nominal/"+variable+"_"+sig+"_"+chs[ch]), variable_range)
        sig_eff = sig_all_num/sig_all_total_num

        for bkg in bkgs:
          bkg_each_num = GetIntegral(region.Get("Nominal/"+variable+"_"+bkg+"_"+chs[ch]), variable_range)
          bkg_all_num += bkg_each_num
        bkg_rej = 1.0 - bkg_all_num / bkg_all_total_num

        print("### Summary ###")
        
        print("All Signal Events: %f" % sig_all_num)
        print("All Singal efficiency: %f" % sig_eff)
        sig_effs.append(sig_eff)

        print("All Background Events: %f" % bkg_all_num)
        print("All Background Rejection: %f" % bkg_rej)
        bkg_rejs.append(bkg_rej)
        
        significance = sig_all_num/math.sqrt(sig_all_num+bkg_all_num)
        print("S/Sqrt(S+B): %f" % significance)
        significances.append(significance)
        print("###############")

        if variable_range[0] != '-inf':
          cuts.append(variable_range[0])
        else:
          cuts.append(variable_range[1])

      ### Make Plots ####
      canvas = ROOT.TCanvas("canvas","canvas",600,600)
      legend = ROOT.TLegend(0.5, 0.12, 0.85, 0.22)
      legend.SetFillColor(0)
      legend.SetBorderSize(0)
      txt = ROOT.TLatex()
      txt.SetNDC(1)
      txt.SetTextFont(132)
      pad1 = ROOT.TPad("pad1","",0,0,1,1) 
      pad2 = ROOT.TPad("pad2","",0,0,1,1)
      pad2.SetFillStyle(4000) #will be transparent
      pad2.SetFrameFillStyle(0) #will be transparent

      ### Singal Efficiency ###
      ncuts = len(cuts)
      cuts = array("f",cuts)
      sig_effs = array("f",sig_effs)
      h_sig = ROOT.TGraph(ncuts,cuts,sig_effs)
      h_sig.SetTitle("")
      h_sig.GetXaxis().SetTitle(variable)
      h_sig.GetYaxis().SetTitleOffset(1.5)
      h_sig.GetYaxis().SetTitle("Rate")
      h_sig.SetMinimum(-0.2)
      h_sig.SetMaximum(1.2)  

      h_sig.SetLineColor(ROOT.kRed)
      h_sig.SetMarkerColor(ROOT.kRed)
      h_sig.SetMarkerStyle(3)
      legend.AddEntry(h_sig, "Signal Efficiency")

      ### Background Rejection ### 

      bkg_rejs = array("f", bkg_rejs)
      h_bkg = ROOT.TGraph(ncuts,cuts,bkg_rejs)
      h_bkg.SetLineColor(ROOT.kBlue)
      h_bkg.SetMarkerColor(ROOT.kBlue)
      h_bkg.SetMarkerStyle(22)
      legend.AddEntry(h_bkg, "Background Rejection")

      ### Significance ###

      significances = array("f", significances)
      h_significance = ROOT.TGraph(ncuts,cuts,significances)
      h_significance.SetTitle("")
      h_significance.GetYaxis().SetTitleOffset(1.5)
      h_significance.GetYaxis().SetTitle("S/Sqrt(S+B)")
      h_significance.SetLineColor(ROOT.kGreen)
      h_significance.SetMarkerColor(ROOT.kGreen)
      h_significance.SetMarkerStyle(12)
      legend.AddEntry(h_significance, "S/Sqrt(S+B)")



      ### Draw ###
      pad1.Draw()
      pad1.cd()
      h_sig.Draw("APC")
      txt.DrawLatex(0.15,0.16,"#font[72]{ATLAS} Internal")
      txt.DrawLatex(0.15,0.12, ch)
      legend.Draw()
      h_sig.Draw("PCSAME")
      h_bkg.Draw("PCSAME")

      # pad1.GetRange(xmin,ymin,xmax,ymax)
      xmin = min(cuts)
      xmax = max(cuts)
      dx = xmax - xmin
      ymin = min(significances)-0.05
      ymax = max(significances)+0.05
      dy = ymax - ymin
      pad2.Range(xmin-0.25*dx,ymin-0.25*dy,xmax+0.25*dx,ymax+0.25*dy)


      pad2.Draw()
      pad2.cd()
      h_significance.Draw("APCSAMEY+")

      # axis = ROOT.TGaxis(xmax,ymin,xmax,ymax,ymin,ymax,50510,"+L");
      # axis->SetLabelColor(kRed);
      # axis->Draw();
      canvas.SaveAs("opt_"+variable+"_"+chs[ch]+".png")




print("--- %s TIME ---" % (time.time() - start_time))





#  LocalWords:  SFOS
