



from ROOT import TColor,gROOT
from math import floor
from collections import namedtuple


WWWColor = namedtuple('WWWColor', 'r g b')

"""
Use Colors taken from Color Brewere
http://colorbrewer2.org/?type=qualitative&scheme=Set1&n=6
"""
ColorMap = {}
ColorMap["Red"] = WWWColor(r=228, g=26, b=28)
ColorMap["LightRed"] = WWWColor(r=255, g=47, b=42)
ColorMap["DarkRed"] = WWWColor(r=156, g=18, b=19)
ColorMap["Blue"] = WWWColor(r=55, g=126, b=184)
ColorMap["LightBlue"] = WWWColor(r=92, g=182, b=245)
ColorMap["DarkBlue"] = WWWColor(r=43, g=100, b=145)
ColorMap["Green"] = WWWColor(r=77, g=175, b=74)
ColorMap["LightGreen"] = WWWColor(r=116, g=238, b=127)
ColorMap["DarkGreen"] = WWWColor(r=28, g=135, b=47)
ColorMap["Purple"] = WWWColor(r=152, g=78, b=163)
ColorMap["LightPurple"] = WWWColor(r=219, g=130, b=227)
ColorMap["DarkPurple"] = WWWColor(r=128, g=66, b=138)
ColorMap["Orange"] = WWWColor(r=255, g=127, b=0)
ColorMap["DarkOrange"] = WWWColor(r=178, g=89, b=0)
ColorMap["Yellow"] = WWWColor(r=255, g=255, b=51)
ColorMap["DarkYellow"] = WWWColor(r=166, g=166, b=33)
ColorMap["Brown"] = WWWColor(r=128, g=98, b=16)
ColorMap["DarkBrown"] = WWWColor(r=102, g=78, b=11)
ColorMap["Grey"] = WWWColor(r=190, g=174, b=212)
ColorMap["DarkGrey"] = WWWColor(r=190, g=174, b=200)
ColorMap["Pink"] = WWWColor(r=244, g=202, b=172)
ColorMap["DarkPink"] = WWWColor(r=244, g=202, b=160)
ColorMap["Skin"] = WWWColor(r=253, g=191, b=111)
ColorMap["DarkSkin"] = WWWColor(r=253, g=191, b=100)


ColorMap["Wjets1"] = WWWColor(r=140, g=150, b=198)
ColorMap["Wjets2"] = WWWColor(r=136, g=86, b=167)
ColorMap["Wjets3"] = WWWColor(r=129, g=15, b=124)

ColorMap["DarkWjets1"] = WWWColor(r=140, g=150, b=198)
ColorMap["DarkWjets2"] = WWWColor(r=136, g=86, b=167)
ColorMap["DarkWjets3"] = WWWColor(r=129, g=15, b=124)


SampleColorMap = {}

#Alternate order
SampleColorMap["WWW"] = "Yellow"
SampleColorMap["WWW2l2j"] = "Yellow"
SampleColorMap["WWW3l"] = "Brown"
SampleColorMap["WH"] = "Brown"

SampleColorMap["WZ"] = "LightPurple"
SampleColorMap["V#gamma"] = "Orange"
SampleColorMap["W#gamma"] = "Orange"
SampleColorMap["Z#gamma"] = "Orange"
SampleColorMap["Fake L."] = "LightBlue"
SampleColorMap["Fakes"] = "LightBlue"
SampleColorMap["Non Prompt"] = "LightBlue"
SampleColorMap["Charge Flip L."] = "LightGreen"
SampleColorMap["Charge Flip"] = "LightGreen"
SampleColorMap["ssWW"] = "Brown"

SampleColorMap["Others"] = "LightRed"

SampleColorMap["ttV"] = "Blue"
SampleColorMap["ttW"] = "Wjets1"
SampleColorMap["ttZ"] = "Wjets2"
SampleColorMap["tZ"] = "Wjets3"



SampleColorMap["ZZ"] = "Grey"
SampleColorMap["VVV"] = "Pink"
SampleColorMap["SingleTop"] = "Skin"

SampleColorMap["ttbar"]="Blue"

SampleColorMap["Wjets"]="Wjets3"
SampleColorMap["Zjets"]="Green"

SampleColorMap["WjetsCVetoBVeto"]="Wjets1"
SampleColorMap["WjetsCFilterBVeto"]="Wjets2"
SampleColorMap["WjetsBFilter"]="Wjets3"


SampleColorMap["ZjetsCVetoBVeto"]="Wjets1"
SampleColorMap["ZjetsCFilterBVeto"]="Wjets2"
SampleColorMap["ZjetsBFilter"]="Wjets3"


# SampleColorMap["WWW"] = "Yellow"
# SampleColorMap["WZ"] = "Orange"
# SampleColorMap["V#gamma"] = "Green"
# SampleColorMap["Fake"] = "Purple"
# SampleColorMap["Charge Flip"] = "Blue"
# SampleColorMap["OtherSS"] = "Red"


startColorIndex = 4000
NewColorIndices = {}
NewColors = {}

def mapColorComponent(hexIndex):
    max = 255
    if hexIndex > max: hexIndex = max
    return float(hexIndex)/float(max)

def getTColorIndex(colorName):
    if colorName in NewColorIndices:
        return NewColorIndices[colorName]
    global startColorIndex
    NewColorIndices[colorName] = startColorIndex
    NewColors[colorName] = TColor(NewColorIndices[colorName],
                                  mapColorComponent(ColorMap[colorName].r),
                                  mapColorComponent(ColorMap[colorName].g),
                                  mapColorComponent(ColorMap[colorName].b))
    startColorIndex+=1
    return NewColorIndices[colorName]


def getColorForCategory(categoryName,lineColor=False):
    modifierTag = ""
    colorName = SampleColorMap[categoryName]
    if lineColor: 
        if "Light" in colorName:
            colorName=colorName.replace("Light","")
        else:
            modifierTag="Dark"

    colorName = modifierTag+colorName
    index = getTColorIndex(colorName)
    color = gROOT.GetColor(index)
    return index


rainbow = []
#rainbow.append(getTColorIndex("Red"))
rainbow.append(getTColorIndex("Orange"))
#rainbow.append(getTColorIndex("Yellow"))
rainbow.append(getTColorIndex("Green"))
#rainbow.append(getTColorIndex("Blue"))
rainbow.append(getTColorIndex("Purple"))



darkRainbow = []
#darkRainbow.append(getTColorIndex("DarkRed"))
darkRainbow.append(getTColorIndex("DarkOrange"))
#darkRainbow.append(getTColorIndex("DarkYellow"))
darkRainbow.append(getTColorIndex("DarkGreen"))
#darkRainbow.append(getTColorIndex("DarkBlue"))
darkRainbow.append(getTColorIndex("DarkPurple"))

lightRainbow = []
lightRainbow.append(getTColorIndex("LightRed"))
lightRainbow.append(getTColorIndex("LightBlue"))
lightRainbow.append(getTColorIndex("LightGreen"))
#lightRainbow.append(getTColorIndex("LightPurple"))
